//---- C O N F I G S ----//

let conf = [
{
    icon : "fa fa-pen-to-square fa-lg",
    icon_color_primary : "#ABB2BF",
    icon_color_secondary : "#85C2E0",
    title : "Productivity",
    description : "Daily apps",
    title_color_primary : "#ABB2BF",
    title_color_secondary : "#cfcfcf",
    description_color_primary : "#c0c0c0",
    description_color_secondary : "#85C2E0",
    content : [
        {
            icon : "fa fa-solid fa-envelope fa-lg",
            href : "https://mail.google.com/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-envelopes-bulk fa-lg",
			href : "https://mail.proton.me/u/3/inbox",
            target : "_self",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-brands fa-google-drive fa-lg",
			href : "https://drive.google.com/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-book fa-lg",
			href : "https://classroom.google.com/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-sitemap fa-lg",
			href : "https://chat.openai.com/",
            target : "_self",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-wave-square fa-lg",
			href : "https://asoftmurmur.com/",
            target : "_self",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		}
	]
},
{
    icon : "fa fa-play fa-lg",
    icon_color_primary : "#ABB2BF",
    icon_color_secondary : "#9485E0",
	title : "Entertainment",
	description : "Downtime and media",
	title_color_primary : "#ABB2BF",
	title_color_secondary : "#cfcfcf",
	description_color_primary : "#c0c0c0",
	description_color_secondary : "#9485E0",
	content : [
		{
			icon : "fa fa-brands fa-youtube fa-lg",
			href : "https://youtube.com/",
            target : "_self",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-brands fa-spotify fa-lg",
			href : "https://open.spotify.com/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-clapperboard fa-lg",
			href : "https://netflix.com/browse/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-plus fa-lg",
			href : "https://disneyplus.com/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		}
	]
},
{
    icon : "fa fa-code fa-lg",
    icon_color_primary : "#ABB2BF",
    icon_color_secondary : "#90E595",
	title : "Projects",
	description : "Tinkerin",
	title_color_primary : "#ABB2BF",
	title_color_secondary : "#cfcfcf",
	description_color_primary : "#c0c0c0",
	description_color_secondary : "#90E595",
	content : [
		{
			icon : "fa fa-solid fa-music fa-lg",
			href : "https://musicforprogramming.net/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-brands fa-gitlab fa-lg",
			href : "https://gitlab.com/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-pen-nib fa-lg",
			href : "https://worldanvil.com/world/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-code fa-lg",
			href : "https://www.freecodecamp.org/learn/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		}
	]
},
{
    icon : "fa fa-bolt fa-lg",
    icon_color_primary : "#ABB2BF",
    icon_color_secondary : "#80C2E5",
	title : "Things",
	description : "Cool stuff or other",
	title_color_primary : "#ABB2BF",
	title_color_secondary : "#cfcfcf",
	description_color_primary : "#c0c0c0",
	description_color_secondary : "#80C2E5",
	content : [
		{
			icon : "fa fa-solid fa-keyboard fa-lg",
			href : "https://monkeytype.com/",
            target : "_self",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-square-minus fa-lg",
			href : "https://keybr.com/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		},
		{
			icon : "fa fa-solid fa-chalkboard-user fa-lg",
			href : "https://www.khanacademy.org/profile/me/courses/",
            target : "_blank",
            color_primary : "#ABB2BF",
            color_secondary : "#cfcfcf"
		}
    ]
}
];

//---- F U N C T I O N S ----//

function fill() {

    //-- M A I N  T A G S --//

    var list = document.querySelector("#list");
    var icons = document.querySelector("#icons");

    for (key in conf) {

        //- L E F T  S I D E  G E N E R A T I O N -//

        // C O N T A I N E R S //

        var olist = document.createElement("div"); 
        var ilist_title = document.createElement("div"); 
        var ilist_icon = document.createElement("div"); 

        // T E X T //

        var title = document.createElement("div"); 
        var description = document.createElement("div"); 

        // L E F T  I C O N //

        var udot = document.createElement("div");
        var title_icon = document.createElement("a");
        var ldot = document.createElement("div");


        olist.id = "list" + (parseInt(key)+1);

        title.id = "title" + (parseInt(key)+1);
        description.id = "description" + (parseInt(key)+1);
        
        udot.id = "udot" + (parseInt(key)+1);
        title_icon.id = "title_icon" + (parseInt(key)+1);
        ldot.id = "ldot" + (parseInt(key)+1);
        

        olist.classList.add("olist");
        ilist_title.classList.add("ilist_title");
        ilist_icon.classList.add("ilist_icon");
        
        title.classList.add("title");
        description.classList.add("description");

        udot.classList.add("udot");
        title_icon.setAttribute("class", conf[key]["icon"]);
        title_icon.classList.add("title_icon");
        ldot.classList.add("ldot");


        title.innerHTML = conf[key]["title"];
        title.style.color = conf[key]["title_color_primary"];

        description.innerHTML = conf[key]["description"];
        description.style.color = conf[key]["description_color_primary"];

        udot.style.backgroundColor = conf[key]["icon_color_primary"];
        title_icon.style.color = conf[key]["icon_color_primary"];
        ldot.style.backgroundColor = conf[key]["icon_color_primary"];


        olist.setAttribute("onmouseover", 'var el1 = document.querySelector("#description'+(parseInt(key)+1)+'"); \
el1.style.color = "'+conf[key]["description_color_secondary"]+'"; \
el1.classList.add("description_active"); \
var el2 = document.querySelector("#title'+(parseInt(key)+1)+'"); \
el2.style.color = "'+conf[key]["title_color_secondary"]+'"; \
el2.classList.add("title_active"); \
this.classList.add("olist_active"); \
var el3 = document.querySelector("#icons'+(parseInt(key)+1)+'"); \
el3.classList.add("icons_active"); \
var icon = document.querySelector("#title_icon'+(parseInt(key)+1)+'"); \
icon.classList.add("title_icon_active"); \
var udot = document.querySelector("#udot'+(parseInt(key)+1)+'"); \
udot.classList.add("udot_active_s"); \
var ldot = document.querySelector("#ldot'+(parseInt(key)+1)+'"); \
ldot.classList.add("ldot_active_s"); \
var icons = document.querySelectorAll("#icons'+(parseInt(key)+1)+' a"); \
for (var i = 0; i<icons.length; i++) { \
icons[i].classList.add("icon_active_b"); \
}');
        olist.setAttribute("onmouseout", 'var el1 = document.querySelector("#description'+(parseInt(key)+1)+'"); \
el1.style.color = "'+conf[key]["description_color_primary"]+'"; \
el1.classList.remove("description_active"); \
var el2 = document.querySelector("#title'+(parseInt(key)+1)+'"); \
el2.style.color = "'+conf[key]["title_color_primary"]+'"; \
el2.classList.remove("title_active"); \
this.classList.remove("olist_active"); \
var el3 = document.querySelector("#icons'+(parseInt(key)+1)+'"); \
el3.classList.remove("icons_active"); \
var icon = document.querySelector("#title_icon'+(parseInt(key)+1)+'"); \
icon.classList.remove("title_icon_active"); \
var udot = document.querySelector("#udot'+(parseInt(key)+1)+'"); \
udot.classList.remove("udot_active_s"); \
var ldot = document.querySelector("#ldot'+(parseInt(key)+1)+'"); \
ldot.classList.remove("ldot_active_s"); \
var icons = document.querySelectorAll("#icons'+(parseInt(key)+1)+' a"); \
for (var i = 0; i<icons.length; i++) { \
icons[i].classList.remove("icon_active_b"); \
}');

        title_icon.setAttribute("onmouseover", "this.classList.add('large_title_icon'); \
this.style.color='"+conf[key]["icon_color_secondary"]+"'; \
var udot = document.querySelector('#udot"+(parseInt(key)+1)+"'); \
udot.classList.add('udot_active_b'); \
udot.style.backgroundColor='"+conf[key]["icon_color_secondary"]+"'; \
var ldot = document.querySelector('#ldot"+(parseInt(key)+1)+"'); \
ldot.classList.add('ldot_active_b'); \
ldot.style.backgroundColor='"+conf[key]["icon_color_secondary"]+"'; \
");
        title_icon.setAttribute("onmouseout", "this.classList.remove('large_title_icon'); \
this.style.color='"+conf[key]["icon_color_primary"]+"'; \
var udot = document.querySelector('#udot"+(parseInt(key)+1)+"'); \
udot.classList.remove('udot_active_b'); \
udot.style.backgroundColor='"+conf[key]["icon_color_primary"]+"'; \
var ldot = document.querySelector('#ldot"+(parseInt(key)+1)+"'); \
ldot.classList.remove('ldot_active_b'); \
ldot.style.backgroundColor='"+conf[key]["icon_color_primary"]+"'; \
");

        //- R I G H T  S I D E  G E N E R A T I O N -//

        // R I G H T  I C O N S  L I S T //

        var icon_list = document.createElement("div");

        icon_list.id = "icons"+(parseInt(key)+1);

        icon_list.classList.add("icon_list");
        
        icon_list.setAttribute("onmouseover", 'var icons = document.querySelectorAll("#icons'+(parseInt(key)+1)+' a"); \
this.classList.add("icons_active"); \
var icon = document.querySelector("#title_icon'+(parseInt(key)+1)+'"); \
icon.classList.add("title_icon_active"); \
var udot = document.querySelector("#udot'+(parseInt(key)+1)+'"); \
udot.classList.add("udot_active_s"); \
var ldot = document.querySelector("#ldot'+(parseInt(key)+1)+'"); \
ldot.classList.add("ldot_active_s"); \
var el1 = document.querySelector("#list'+(parseInt(key)+1)+'"); \
el1.classList.add("olist_active"); \
for (var i = 0; i<icons.length; i++) { \
icons[i].classList.add("icon_active_s"); \
}');
        icon_list.setAttribute("onmouseout", 'var icons = document.querySelectorAll("#icons'+(parseInt(key)+1)+' a"); \
this.classList.remove("icons_active"); \
var icon = document.querySelector("#title_icon'+(parseInt(key)+1)+'"); \
icon.classList.remove("title_icon_active"); \
var udot = document.querySelector("#udot'+(parseInt(key)+1)+'"); \
udot.classList.remove("udot_active_s"); \
var ldot = document.querySelector("#ldot'+(parseInt(key)+1)+'"); \
ldot.classList.remove("ldot_active_s"); \
var el1 = document.querySelector("#list'+(parseInt(key)+1)+'"); \
el1.classList.remove("olist_active"); \
for (var i = 0; i<icons.length; i++) { \
icons[i].classList.remove("icon_active_s"); \
}');

        // M U L T I P L E  L I N K S  O P E N I N G //

        var multiple = "";

        for (icon in conf[key]["content"]) {
            multiple += "window.open('"+conf[key]["content"][icon]["href"]+"');";

            // L I N K //

            var link = document.createElement("a");

            link.href = conf[key]["content"][icon]["href"];

            link.classList.add("icon");

            link.style.transition = "transform 0.3s cubic-bezier(0.95,-0.6, 0.45, 2) "+((conf[key]["content"].length-icon+1-1)*0.02)+"s";

            link.setAttribute("target", conf[key]["content"][icon]["target"]);
            link.setAttribute("onmouseover", "this.classList.add('large_icon');this.firstChild.style.color='"+conf[key]["content"][icon]["color_secondary"]+"';");
            link.setAttribute("onmouseout", "this.classList.remove('large_icon');this.firstChild.style.color='"+conf[key]["content"][icon]["color_primary"]+"';");
            
            // I C O N //

            var faicon = document.createElement("div");
            
            faicon.style.color = conf[key]["content"][icon]["color_primary"];
            faicon.style.transition = "color 0.2s cubic-bezier(0.5, 0.25, 0.6, 1.0) "+((conf[key]["content"].length-icon+1-1)*0.02+0.05)+"s";
            
            faicon.setAttribute("class", conf[key]["content"][icon]["icon"]);

            link.appendChild(faicon);
            icon_list.appendChild(link);
        }

        title_icon.setAttribute("onclick", multiple);

        //- F I L L I N G  M A I N  T A G S -//

        ilist_title.appendChild(title);
        ilist_title.appendChild(description);
        ilist_icon.appendChild(udot);
        ilist_icon.appendChild(title_icon);
        ilist_icon.appendChild(ldot);
        olist.appendChild(ilist_title);
        olist.appendChild(ilist_icon);
        list.appendChild(olist);

        icons.appendChild(icon_list);
    }
}

//---- M A I N ----//

fill();
