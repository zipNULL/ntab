w# Startpage

Simple startpage

## Table of content : 

- [General info](#general-info)
- [Preview](#preview)
- [Usage](#usage)

# General info

This is my fork of a startpage with parallax background (not currently using that funcitonality). It's written in plain `HTML / CSS / JS`, so you can copy it without any preparations and building. Inspired by [this startpage](https://github.com/CodeHeister/parallax_list). 

# Preview

![Preview](preview.jpg)

# Usage

1. Download `index.html` and `src` folder
2. Configure `var conf` in `src/js/main.js`
3. Launch `index.html`
